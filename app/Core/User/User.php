<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-24
 * Time: 22:21
 */

namespace Core\User;
use Illuminate\Database\Eloquent\Model as Model;

class User extends Model {

    protected $table = 'users';
    protected  $fillable = [
	'first_name', 'last_name', 'email', 'username', 'password', 'active', 'active_hash', 'remember_identifier', 'remember_token'
    ];

    public function __construct(array $attributes = array()){
	parent::__construct($attributes);
    }

    public function getFirstName(){
	return $this->first_name;
    }

    public function getLastName(){
	return $this->last_name;
    }

    public function getEmail(){
	return $this->email;
    }

    public function getUsername(){
	return $this->username;
    }


}