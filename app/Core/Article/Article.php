<?php

/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-26
 * Time: 22:07
 */
namespace Core\Article;
use Illuminate\Database\Eloquent\Model as Model;

class Article extends Model {

    protected $table = 'article';
    protected  $fillable = [
	'author', 'title', 'body', 'status'
    ];

    public function __construct(array $attributes = array()){
	parent::__construct($attributes);
    }

    public function getTitle(){
	return $this->title;
    }

    public function getBody(){
	return $this->body;
    }

    public function getAuthor(){
	return $this->author;
    }
    public function getStatus(){
	return (bool)$this->status;
    }
}